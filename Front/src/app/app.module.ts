import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
// COMPONENTS
import { MovieDetailComponent } from './pages/movie-detail/movie-detail.component';
import { NavbarComponent } from './partials/navbar/navbar.component';
import { HomeComponent } from './pages/home/home.component';
import { HomeHeaderComponent } from './components/home-header/home-header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BookingComponent } from './pages/booking/booking.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { DashboardReservationComponent } from './components/dashboard-reservation/dashboard-reservation.component';
import { DashboardReviewComponent } from './components/dashboard-review/dashboard-review.component';
import { DashboardPersonnalDataComponent } from './components/dashboard-personnal-data/dashboard-personnal-data.component';
import { ModalReviewComponent } from './components/modals/modal-review/modal-review.component';
// MATERIAL
import { MaterialModule } from './material/material.module';
// MODALES
import { ModalTrailerComponent } from './components/modals/modal-trailer/modal-trailer.component';
import { ModalRegisterComponent } from './components/modals/modal-register/modal-register.component';
import { ModalLoginComponent } from './components/modals/modal-login/modal-login.component';
// QR CODE
import { QRCodeModule } from 'angularx-qrcode';
import { MovieDetailCastingComponent } from './components/movie-detail-casting/movie-detail-casting.component';
import { MovieDetailReviewComponent } from './components/movie-detail-review/movie-detail-review.component';

// PIPE
import { DisplayDurationPipe } from './pipes/display-duration.pipe';
import { DisplayRatePipe } from './pipes/display-rate.pipe';
import { OngoingMoviesPipe } from './pipes/ongoing-movies.pipe';
import { UpcomingMoviesPipe } from './pipes/upcoming-movies.pipe';

// PRIMENG
import {CarouselModule} from 'primeng/carousel';
import {ButtonModule} from 'primeng/button';
import {ToastModule} from 'primeng/toast';
import {AuthInterceptor} from './services/helpers/authconfig.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    ModalTrailerComponent,
    MovieDetailComponent,
    DisplayRatePipe,
    HomeHeaderComponent,
    ModalRegisterComponent,
    ModalLoginComponent,
    BookingComponent,
    DashboardComponent,
    DashboardReservationComponent,
    DashboardReviewComponent,
    DashboardPersonnalDataComponent,
    DisplayDurationPipe,
    UpcomingMoviesPipe,
    OngoingMoviesPipe,
    MovieDetailCastingComponent,
    MovieDetailReviewComponent,
    ModalReviewComponent,
    OngoingMoviesPipe,
    UpcomingMoviesPipe
  ],
  entryComponents:[
    ModalTrailerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    QRCodeModule,
    CarouselModule,
    ButtonModule,
    ToastModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
