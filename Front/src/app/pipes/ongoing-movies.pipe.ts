import {Pipe, PipeTransform} from '@angular/core';
import {Movie} from '../Models/movie';

@Pipe({
  name: 'ongoingMovies'
})
export class OngoingMoviesPipe implements PipeTransform {

  transform(movies: Array<Movie>): Array<Movie> {
    if (movies) {
      // .getTime() converi la date en millisecond
      const currentDate = new Date().getTime();
      /**
       * On itere sur les films avec filter
       * On itere sur chaque element avec some pour verifie si la condition est vrai afin de retourner les films ongoing
       */
      return movies.filter((movie: Movie) => {
        const movieOngoing: boolean = movies.some(() => currentDate >= new Date(movie.start_date).getTime());
        return movieOngoing;
      });
    }
    return [];
  }
}
