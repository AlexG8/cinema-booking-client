import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'displayDuration'
})
export class DisplayDurationPipe implements PipeTransform {

  transform(duration): string {
    const hours = (duration / 60);
    const rhours = Math.floor(hours);
    const minutes = (hours - rhours) * 60;

    return `${rhours}h ${minutes.toFixed(0)}min`;
    }

}
