import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'displayRate'
})
export class DisplayRatePipe implements PipeTransform {

  transform(rate: any): Array<any> {
    const rating: any = rate ? Math.ceil(rate / 0.5) * 0.5 : 0;
    let starsArray: Array<any> = Array(5).fill(null);
    if (rating === 0) {
      starsArray = starsArray.map((index) => {
        return {
          classIcon: 'far fa-star'
        };
      });
    }
    else if (!!(rating % 1)) {
      starsArray = starsArray.map((rates, index) => {
        return {
          classIcon:
            index === rating - 0.5 ? 'fas fa-star-half yellow' :
              rating > index ? 'fas fa-star yellow' :
                rating < index ? 'far fa-star' :
                  null
        };
      });
    }
    else {
      starsArray = starsArray.map((rates, index) => {
        return {
          classIcon:
            rating > index ? 'fas fa-star yellow' : 'far fa-star',
        };
      });
    }
    return starsArray;
  }
}
