export interface Screening {
  format: string;
  id: number;
  movie_id: number;
  nb_spectator: number;
  number: number;
  occupied: boolean;
  room_id: number;
  row: string;
  screening_date: Date;
  screening_id: number;
  screening_start: Date;
  seat_id: number;
  seat_statut: string;
  user_id: number;
}
