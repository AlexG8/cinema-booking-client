export interface Review {
  id: number;
  rating: number;
  title: string;
  comment: string;
  likes: number;
  created_at: Date;
  user_id: number;
  movie_id: number;
  alreadyLike: boolean;
  username: string;
  avatar: string;
  data?: any;
  poster?: string;
}
