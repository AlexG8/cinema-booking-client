export interface Movie {
  data: Array<any>;
  id: number;
  original_title: string;
  fr_title: string;
  country: string;
  synopsis: string;
  duration: number;
  poster: string;
  trailer: string;
  year: number;
  start_date: Date;
  end_date: Date;
  label: string;
}
