import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MovieDetailComponent } from './pages/movie-detail/movie-detail.component';
import { HomeComponent } from './pages/home/home.component';
import {BookingComponent} from './pages/booking/booking.component';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {DashboardReservationComponent} from './components/dashboard-reservation/dashboard-reservation.component';
import {DashboardReviewComponent} from './components/dashboard-review/dashboard-review.component';
import {DashboardPersonnalDataComponent} from './components/dashboard-personnal-data/dashboard-personnal-data.component';
import {AuthGuard} from './services/helpers/auth.guard';

const routes: Routes = [
  // Route guest
  {
    path: '',
    redirectTo: '/accueil',
    pathMatch: 'full'
  },
  {
    path: 'accueil',
    component: HomeComponent
  },
  {
    path: 'accueil/movie/:id',
    component: MovieDetailComponent
  },
  // Route Logged
  {
    path: 'accueil/booking/:id',
    component: BookingComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'accueil/dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'reservation', component: DashboardReservationComponent},
      { path: 'review', component: DashboardReviewComponent},
      { path: 'profile', component: DashboardPersonnalDataComponent},
    ],
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
