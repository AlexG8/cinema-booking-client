import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/users/auth.service';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-modal-login',
  templateUrl: './modal-login.component.html',
  styleUrls: ['./modal-login.component.scss']
})
export class ModalLoginComponent implements OnInit {
  loginForm: FormGroup = this.fb.group({
    username: ['', Validators.compose([Validators.required])],
    password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
  });
  loginError: boolean;

  constructor(private fb: FormBuilder, private authService: AuthService,
              public dialogRef: MatDialogRef<ModalLoginComponent>) { }

  ngOnInit(): void {
  }

  authenticate(): void {
    this.authService.login(this.loginForm.value).subscribe(
      (res: any) => {
        this.loginError = false;
        this.dialogRef.close(res.data.user);
      },
      (err) => {
        console.log(err);
        this.loginError = true;
        return err;
      }
    );
  }
}
