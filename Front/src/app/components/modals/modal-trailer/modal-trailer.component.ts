import { Component, OnInit, Input } from '@angular/core';
import {DomSanitizer,SafeResourceUrl,} from '@angular/platform-browser';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-trailer',
  templateUrl: './modal-trailer.component.html',
  styleUrls: ['./modal-trailer.component.scss']
})
export class ModalTrailerComponent implements OnInit {
  @Input() Trailer;

  constructor(public activeModal: NgbActiveModal, public sanitizer: DomSanitizer
  ) {
  }

  ngOnInit(): void {     
  }

  closeModal() {
    this.activeModal.close();
  }

}
