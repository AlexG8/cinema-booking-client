import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AuthService} from '../../../services/users/auth.service';
import {ReviewService} from '../../../services/review.service';

@Component({
  selector: 'app-modal-review',
  templateUrl: './modal-review.component.html',
  styleUrls: ['./modal-review.component.scss']
})
export class ModalReviewComponent implements OnInit {
  currentUser: any;

  reviewForm: FormGroup = this.fb.group({
    rating: ['', Validators.compose([Validators.required])],
    title: ['', Validators.compose([Validators.required])],
    comment: ['', Validators.compose([Validators.required])],
  });
  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<ModalReviewComponent>, private authService: AuthService,
              @Inject(MAT_DIALOG_DATA) public data: {movie_id?: number, review_id?: number}, private reviewService: ReviewService) {
    this.currentUser = JSON.parse(this.authService.user);
  }

  ngOnInit(): void {
  }

  submitReview(): void {
    // on post ou on update suivant la data qu'on a passe a la modal
    if (this.data.movie_id) {
      const body = {
        rating: this.reviewForm.value.rating,
        title: this.reviewForm.value.title,
        comment: this.reviewForm.value.comment,
        user_id: this.currentUser.data.user.id,
        movie_id: this.data.movie_id
      };
      this.postReview(body);
    } else if (this.data.review_id) {
      const body = {
        rating: this.reviewForm.value.rating,
        title: this.reviewForm.value.title,
        comment: this.reviewForm.value.comment,
        user_id: this.currentUser.data.user.id,
        review_id: this.data.review_id
      };
      this.updateReview(body);
    }
  }

  postReview(body): void {
    this.reviewService.postReview(body).subscribe(
      (res: any) => {
        this.dialogRef.close(res.body);
      },
      (err) => err
    );
  }

  updateReview(body): void {
    this.reviewService.updateReview(body).subscribe((res: any) => {
      console.log(res);
      console.log(res.body);
      this.dialogRef.close(res.body);
    }, (err) => err);
  }
}
