import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AuthService} from '../../../services/users/auth.service';
import {UploadImageService} from '../../../services/upload-image.service';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-modal-register',
  templateUrl: './modal-register.component.html',
  styleUrls: ['./modal-register.component.scss']
})
export class ModalRegisterComponent implements OnInit {
  selectedFiles: FileList;
  hide = true;
  file: File;
  filekey: string;
  showError: boolean;


  registerForm: FormGroup = this.fb.group({
    username: ['', Validators.compose([Validators.required])],
    email: ['', Validators.compose([Validators.required, Validators.email])],
    password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
    avatar: ['', Validators.compose([Validators.required])],
  });

  constructor(private fb: FormBuilder,
              public dialog: MatDialog,
              private authService: AuthService,
              private uploadImageService: UploadImageService) { }

  ngOnInit(): void {
  }

  selectFile(event): void {
    this.selectedFiles = event.target.files;
    this.file = this.selectedFiles.item(0);
    this.filekey = environment.FOLDER + (Date.now() + this.selectedFiles.item(0).name);
    this.registerForm.controls.avatar.setValue(environment.FILE_URL + this.filekey);
  }

  register(): void {
    this.showError = false;
    this.uploadImageService.uploadFile(this.file, this.filekey);
    this.authService.register(this.registerForm.value).subscribe(
      () => {
        this.dialog.closeAll();
        // return user;
      },
      (err) => {
        this.showError = true;
        return err;
      }
    );
  }


}
