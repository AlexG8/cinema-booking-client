import { Component, OnInit } from '@angular/core';
import {ReviewService} from '../../services/review.service';
import {AuthService} from '../../services/users/auth.service';
import { Review } from '../../Models/review';
import {ModalReviewComponent} from '../modals/modal-review/modal-review.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-dashboard-review',
  templateUrl: './dashboard-review.component.html',
  styleUrls: ['./dashboard-review.component.scss']
})
export class DashboardReviewComponent implements OnInit {
  currentUser: any;
  reviews: Array<Review>;

  constructor(private reviewService: ReviewService, private authService: AuthService, public dialog: MatDialog) {
    this.currentUser = JSON.parse(this.authService.user);
  }

  ngOnInit(): void {
    this.getReviewsUser();
  }

  getReviewsUser(): void {
    this.reviewService.getReviewsUser(this.currentUser.data.user.id).subscribe((res: any) => this.reviews = res.data);
  }

  deleteUserReview(id): void {
    this.reviewService.deleteReview(id)
      .subscribe(() => this.reviews = this.reviews.filter(review => review.id !== id));
  }

  openModalReview(id): void {
    const modalRef = this.dialog.open(ModalReviewComponent, {
      data: { review_id: id },
    });

    modalRef.afterClosed().subscribe((result: any) => {
      console.log(result);
      if (result) {
        this.getReviewsUser();
      }
    });
  }

}
