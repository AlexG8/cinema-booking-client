import {Component, OnInit} from '@angular/core';
import {MoviesService} from '../../services/movies.service';
import {AuthService} from '../../services/users/auth.service';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-dashboard-reservation',
  templateUrl: './dashboard-reservation.component.html',
  styleUrls: ['./dashboard-reservation.component.scss']
})
export class DashboardReservationComponent implements OnInit {
  currentUser: any;
  bookings: Array<any>;

  constructor(private movieService: MoviesService, private authService: AuthService) {
    this.currentUser = JSON.parse(this.authService.user);
  }

  ngOnInit(): void {
    this.fetchBooking();
  }

  fetchBooking(): void {
    this.movieService.getBooking(this.currentUser.data.user.id).subscribe((res: any) => this.bookings = res.data);
  }

  cancelBooking(id, seatId, screeningId): void {
    // Remettre le siege dispo
    const body = {
        seat_id: seatId,
        screening_id: screeningId
      };
    this.movieService.seatAvailable(body).subscribe(() => {
       console.log('Siege dispo');
       this.movieService.increaseNbOfScreeningSeats({screening_id: screeningId}).toPromise();
       this.movieService.desactivateBooking(id).subscribe(() => {
          this.movieService.deleteBooking(id).subscribe(() => {
            this.fetchBooking();
          });
       });
     });
  }

  getBase64Image(img): string {
    const canvas = document.createElement('canvas');
    canvas.width = img.width;
    canvas.height = img.height;
    const ctx = canvas.getContext('2d');
    ctx.drawImage(img, 0, 0);
    return canvas.toDataURL('image/png');
  }

  exportPDF(divId: number, booking: any): void {
    const qrcode = document.getElementById('qrcode');
    const data = document.getElementById(divId.toString());
    html2canvas(data).then(canvas => {
      const doc = new jsPDF({
        orientation: 'landscape',
        unit: 'in',
        format: [8, 4]
      });
      const imageData = this.getBase64Image(qrcode.firstChild.firstChild);
      doc.addImage(imageData, 'JPG', 0, 0, 0, 0);
      doc.text(`Voici votre billet pour le film ${booking.fr_title},
      votre siege sera le ${booking.row}${booking.number},
      Le ${booking.screening_date} a  ${booking.screening_start}
      `, 1, 2);
      doc.save(`billet-${divId}.pdf`);
    });
  }
}
