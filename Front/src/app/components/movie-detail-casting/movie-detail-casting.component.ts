import {Component, Input, OnInit} from '@angular/core';
import {MoviesService} from '../../services/movies.service';

@Component({
  selector: 'app-movie-detail-casting',
  templateUrl: './movie-detail-casting.component.html',
  styleUrls: ['./movie-detail-casting.component.scss']
})
export class MovieDetailCastingComponent implements OnInit {
  /**
   * Recupere l'id du film via le composant movie-detail
   */
  @Input() idMovie;

  /**
   * Initie la variable qui va recuperer les acteurs
   * TO-DO creer un model
   */
  actors: Array<any>;

  constructor(private movieService: MoviesService) { }

  ngOnInit(): void {
    this.getMovieActors();
  }

  /**
   * Appelle le service qui appelle la route Fetchant les donnees pour recuperer la liste des acteurs correspondant
   * au film via son id
   */
  getMovieActors(): void {
    this.movieService.getMovieActors(this.idMovie).subscribe((res: any) => this.actors = res.data);
  }
}
