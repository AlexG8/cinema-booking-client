import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieDetailCastingComponent } from './movie-detail-casting.component';

describe('MovieDetailCastingComponent', () => {
  let component: MovieDetailCastingComponent;
  let fixture: ComponentFixture<MovieDetailCastingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovieDetailCastingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieDetailCastingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
