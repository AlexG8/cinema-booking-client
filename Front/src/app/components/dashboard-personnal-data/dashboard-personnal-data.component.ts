import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import {UploadImageService} from '../../services/upload-image.service';
import {UserService} from '../../services/users/user.service';
import {AuthService} from '../../services/users/auth.service';

@Component({
  selector: 'app-dashboard-personnal-data',
  templateUrl: './dashboard-personnal-data.component.html',
  styleUrls: ['./dashboard-personnal-data.component.scss']
})
export class DashboardPersonnalDataComponent implements OnInit {
  currentUser: any;
  selectedFiles: FileList;
  file;
  filekey: string;
  url: string;


  constructor(private uploadImageService: UploadImageService, private userService: UserService, private authService: AuthService) {
    this.currentUser = JSON.parse(this.authService.user);
  }

  ngOnInit(): void {
  }

  selectFile(event): void {
    console.log(event);
    this.selectedFiles = event.target.files;
    this.file = this.selectedFiles.item(0);
    console.log(this.selectedFiles.item);
    this.filekey = environment.FOLDER + (Date.now() + this.selectedFiles.item(0).name);

    this.url = environment.FILE_URL + this.filekey;
    const avatarUrl = (environment.FILE_URL + this.filekey);
    const url = {
      avatarUrl
    };
    console.log(url);
    this.uploadImageService.uploadFile(this.file, this.filekey).then(() => {
      this.userService.updateAvatar(this.currentUser.data.user.id, url).subscribe((res: any) => {
        this.currentUser.data.user.avatar = res.message;
        this.authService.storeCurrentUser(this.currentUser);
        localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
      });
    }).catch((err) => console.log(err));
  }

}
