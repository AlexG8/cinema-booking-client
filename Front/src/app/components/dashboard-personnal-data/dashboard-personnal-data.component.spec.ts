import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardPersonnalDataComponent } from './dashboard-personnal-data.component';

describe('DashboardPersonnalDataComponent', () => {
  let component: DashboardPersonnalDataComponent;
  let fixture: ComponentFixture<DashboardPersonnalDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardPersonnalDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardPersonnalDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
