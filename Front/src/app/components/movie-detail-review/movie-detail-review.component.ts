import {Component, Input, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ModalReviewComponent} from '../modals/modal-review/modal-review.component';
import {ReviewService} from '../../services/review.service';
import { Review } from '../../Models/review';
import {AuthService} from '../../services/users/auth.service';
import {LikeService} from '../../services/like.service';

@Component({
  selector: 'app-movie-detail-review',
  templateUrl: './movie-detail-review.component.html',
  styleUrls: ['./movie-detail-review.component.scss']
})
export class MovieDetailReviewComponent implements OnInit {
  /**
   * Recupere l'id du film via le composant movie-detail
   */
  @Input() idMovie;

  /**
   * Recupere l'id de l'utilisateur connecter via le composant movie-detail
   */
  @Input() idUser;

  currentUser: any;

  reviewsMovie: Array<Review>;

  /**
   *  Defini on est en train de filtrer les notes
   */
  filterActif: boolean;
  showAddReviewButton = true;

  /**
   * Stocke le nombre de vote par valeur dans chaque index
   */
  reviewRateArray: Array<number> = Array(5).fill(0);

  /**
   * stock le pourcentage dans chaque index
   */
  percentRateArray: Array<number> = Array(5).fill(0);

  arrayLikes: Array<any> = [];

  constructor(public dialog: MatDialog, public authService: AuthService, private reviewService: ReviewService,
              private likeService: LikeService) {
    this.currentUser = JSON.parse(this.authService.user);
  }

  ngOnInit(): void {
    this.fetchMovieReview();
  }

  /**
   * compte le nombre de vote par valeur et les stock dans les 5 index du tableau reviewArray
   * reviewArray a l'initialisation => [0, 0, 0, 0 ,0]
   */
  countRate(): void {
    if (this.reviewsMovie && this.reviewsMovie.length > 0) {
      this.reviewRateArray = this.reviewsMovie.reduce((review, rateActif) => {
        review[rateActif.rating - 1]++;
        return review;
      }, this.reviewRateArray).reverse();
      this.displayProgressBar();
    }
  }

  /**
   * dans les 5 index du tableau percentArray stock le resultat de la fonction percentRate
   */
  displayProgressBar(): void {
    this.percentRateArray = this.reviewRateArray.map((reviewActif) =>
      this.percentRate(reviewActif));
  }

  /**
   * Calcul le pourcentage de note par value (1, 2, 3, 4, 5) par rapport au total de note
   */
  percentRate(rate: number): number {
    return (rate / this.reviewsMovie.length) * 100;
  }

  /**
   * Recupere la valeur de la note au clique et filtre les reviews par rate
   */
  filterByRate(rate: number): void {
    this.filterActif = true;
    this.reviewService.getReviewByRate(rate).subscribe((res: any) => this.reviewsMovie = res.data);
  }

  desactivateRateFilter(): void {
    this.reviewService.getReviews(this.idMovie).subscribe((res: any) => this.reviewsMovie = res.data);
    this.filterActif = false;
  }

  fetchMovieReview(): void {
    this.reviewService.getReviews(this.idMovie).subscribe((res: any) => {
      if (res.data) {
        this.reviewsMovie = res.data;
        if (this.authService.isLoggedIn) {
          const isUnique = this.reviewsMovie.some(rw => rw.user_id === this.idUser.data.user.id);
          this.showAddReviewButton = !isUnique;
        }
        this.countRate();
      }
    });
  }

  openModalReview(): void {
    const modalRef = this.dialog.open(ModalReviewComponent, {
      data: { movie_id: this.idMovie },
    });

    modalRef.afterClosed().subscribe((result: any) => {
      console.log(result);
      if (result) {
        this.fetchMovieReview();
      }
    });
  }

  likeReview(idReview): void {
    if (this.authService.isLoggedIn) {
      this.reviewService.getLike(idReview).subscribe((res: Review) => {
        const body = {
          id_user: this.currentUser.data.user.id,
          id_review: idReview,
          id_movie: parseInt(this.idMovie, 0)
        };
        this.arrayLikes = res.data;
        if (this.arrayLikes && this.arrayLikes.length > 0) {
          const userAlreadyLike = this.arrayLikes.some(dd => dd.id_user === this.currentUser.data.user.id);
          if (userAlreadyLike) {
            this.deleteLike(body);
          } else {
            this.reviewService.likeReview(body).subscribe(() => {
              this.incrementLike(idReview);
            });
          }
        } else if (this.arrayLikes && this.arrayLikes.length === 0) {
          this.reviewService.likeReview(body).subscribe(() => {
            this.incrementLike(idReview);
          });
        }
      });
    }
  }

  deleteLike(body): void {
    this.reviewService.deleteLike(body).subscribe(() => {
      this.decrementLike(body.id_review);
    });
  }

  incrementLike(idReview): void {
    const body = {
      id_review: idReview,
    };
    this.likeService.incrementLike(body).subscribe(() => {
      this.reviewsMovie.forEach((review: Review) => {
        if (review.id === idReview) {
          review.likes++;
          review.alreadyLike = true;
        }
      });
    });
  }

  decrementLike(idReview): void {
    const body = {
      id_review: idReview,
    };
    this.likeService.decrementLike(body).subscribe(() => {
      this.reviewsMovie.forEach((review: Review) => {
        if (review.id === idReview) {
          review.likes--;
          review.alreadyLike = false;
        }
      });
    });
  }
}
