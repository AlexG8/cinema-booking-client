import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieDetailReviewComponent } from './movie-detail-review.component';

describe('MovieDetailReviewComponent', () => {
  let component: MovieDetailReviewComponent;
  let fixture: ComponentFixture<MovieDetailReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovieDetailReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieDetailReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
