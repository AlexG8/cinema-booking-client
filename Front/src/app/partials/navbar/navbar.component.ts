import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/users/auth.service';
import {MatDialog} from '@angular/material/dialog';
import {ModalRegisterComponent} from '../../components/modals/modal-register/modal-register.component';
import {ModalLoginComponent} from '../../components/modals/modal-login/modal-login.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  currentUser: object;
  constructor(public authService: AuthService, private router: Router, public dialog: MatDialog) {
    this.currentUser = JSON.parse(this.authService.user);
  }

  openModalRegister(): void {
    this.dialog.open(ModalRegisterComponent);
  }

  openModalLogin(): void {
    const modalRef = this.dialog.open(ModalLoginComponent);

    modalRef.afterClosed().subscribe((result: any) => {
      if (result) {
        this.currentUser = JSON.parse(this.authService.user);
      }
    });
  }

  logout(): void {
    this.currentUser = null;
    this.authService.logout();
  }
}
