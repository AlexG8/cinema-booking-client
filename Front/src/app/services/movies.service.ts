import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {
  apiURL = `${environment.API_URL}/api/movie/`;

  constructor(private http: HttpClient) { }

  getMovies(): Observable<object> {
    return this.http.get(this.apiURL);
  }

  getMovie(id: number): Observable<object> {
    return this.http.get(`${this.apiURL}${id}`);
  }

  getMovieActors(movieId: number): Observable<object> {
    return this.http.get(`${this.apiURL}actors/${movieId}`);
  }

  getMovieGenres(): Observable<object> {
    return this.http.get(`${this.apiURL}genres`);
  }

  getMovieScreenings(idMovie: number): Observable<object> {
    return this.http.get(`${this.apiURL}screenings/${idMovie}`);
  }

  getMoviesByFormatAndDate(body: object): Observable<object> {
    return this.http.post(`${this.apiURL}filter`, body);
  }

  getScreening(id: number): Observable<object> {
    return this.http.get(`${this.apiURL}screening/${id}`);
  }

  bookScreening(body): Observable<object> {
    return this.http.post(`${this.apiURL}screening`, body);
  }

  seatOccupied(body): Observable<object> {
    return this.http.put(`${this.apiURL}seat/occupied`, body);
  }

  seatSelected(body): Observable<object> {
    return this.http.put(`${this.apiURL}seat/selected`, body);
  }

  seatAvailable(body): Observable<object> {
    return this.http.put(`${this.apiURL}seat/available`, body);
  }

  nbOfScreeningSeats(body): Observable<object> {
    return this.http.put(`${this.apiURL}screening/spectator`, body);
  }

  increaseNbOfScreeningSeats(body): Observable<object> {
    return this.http.put(`${this.apiURL}screening/spectator/increase`, body);
  }

  getBooking(userId): Observable<object> {
    return this.http.get(`${this.apiURL}booking/${userId}`);
  }

  deleteBooking(id): Observable<object> {
    return this.http.delete(`${this.apiURL}booking/${id}`);
  }

  desactivateBooking(id): Observable<object> {
    const body = {active: 0};
    return this.http.put(`${this.apiURL}booking/${id}`, body);
  }

  getReviews(): Observable<object> {
    return this.http.get(`${this.apiURL}review`);
  }
}
