import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {
  apiURL = `${environment.API_URL}/api/review/`;

  constructor(private http: HttpClient) { }

  getReviews(idMovie): Observable<object> {
    return this.http.get(`${this.apiURL}movie/${idMovie}`);
  }

  getReviewsUser(id): Observable<object> {
    return this.http.get(`${this.apiURL}user/${id}`);
  }

  getReviewByRate(rate): Observable<object> {
    return this.http.get(`${this.apiURL}${rate}`);
  }

  postReview(body): Observable<object> {
    return this.http.post(this.apiURL, body);
  }

  updateReview(body): Observable<object> {
    return this.http.put(`${this.apiURL}${body.review_id}`, body);
  }

  deleteReview(id): Observable<object> {
    return this.http.delete(`${this.apiURL}${id}`);
  }

  getLike(id): Observable<object> {
    return this.http.get(`${this.apiURL}like/${id}`);
  }

  likeReview(body): Observable<object> {
    return this.http.post(`${this.apiURL}like`, body);
  }

  deleteLike(body): Observable<object> {
    return this.http.post(`${this.apiURL}like/delete`, body);
  }
}
