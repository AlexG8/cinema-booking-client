import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { catchError, mapTo, tap } from 'rxjs/operators';
import {Observable} from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  apiURL = `${environment.API_URL}/api/user/`;
  user: any;

  constructor(private http: HttpClient, private router: Router) {
    this.user = localStorage.getItem('currentUser');
   }

  register(user): Observable<any> {
    return this.http.post(`${this.apiURL}register`, user);
  }

  login(body): Observable<any> {
    return this.http.post(`${this.apiURL}authenticate`, body).pipe(
      tap((user: object) => this.storeCurrentUser(user))
    );
  }

  get isLoggedIn(): boolean {
    const user = localStorage.getItem('currentUser');
    return (user !== null);
  }

  logout(): void {
    localStorage.removeItem('currentUser');
    this.user = null;
    this.router.navigate(['accueil']).then(r => r);
  }

  storeCurrentUser(user: object): void {
    localStorage.setItem('currentUser', JSON.stringify(user));
    this.user = localStorage.getItem('currentUser');
  }
}
