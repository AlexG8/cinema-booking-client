import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  apiURL = `${environment.API_URL}/api/user/`;

  constructor(private http: HttpClient) { }

  getBookedMovie(body: object): Observable<object> {
    return this.http.post(`${this.apiURL}booked`, body);
  }

  postReview(body: object): Observable<object> {
    return this.http.post(`${this.apiURL}review`, body);
  }

  getLikes(movieId: number): Observable<object> {
    return this.http.get(`${this.apiURL}likes/${movieId}`);
  }

  getMyLikesByReview(reviewId: number, userId: number,  movieId: number): Observable<object> {
    return this.http.get(`${this.apiURL}likes/${reviewId}/${userId}/${movieId}`);
  }

  postReviewLikes(body: object): Observable<object> {
    return this.http.post(`${this.apiURL}likes`, body);
  }

  updateAvatar(userId, url): Observable<object> {
    return this.http.put(`${this.apiURL}editAvatar/${userId}`, url);
  }

  deleteLikes(userId: number, reviewId: number, movieId: number): Observable<object>  {
    return this.http.delete(`${this.apiURL}likes/${userId}/${reviewId}/${movieId}`);
  }
}
