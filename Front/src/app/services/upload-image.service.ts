import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';
import {ManagedUpload} from 'aws-sdk/lib/s3/managed_upload';
import SendData = ManagedUpload.SendData;

@Injectable({
  providedIn: 'root'
})
export class UploadImageService {
  constructor() {
  }

  uploadFile(file, key): Promise<any> {
    return new Promise((resolve, reject) => {
      const contentType = file.type;
      const bucket = new S3(
        {
          accessKeyId: environment.ACCESS_KEY,
          secretAccessKey: environment.SECRET_KEY,
          region: 'eu-west-3'
        }
      );
      const params = {
        Bucket: environment.BUCKET,
        Key: key,
        Body: file,
        ACL: 'public-read',
        ContentType: contentType
      };
      return bucket.upload(params, (err: Error, data: SendData) => {
        if (err) {
          reject(err);
        }
        resolve(data);
      });
    });
  }

}
