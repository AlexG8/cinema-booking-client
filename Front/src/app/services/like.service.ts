import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LikeService {
  apiURL = `${environment.API_URL}/api/like/`;

  constructor(private http: HttpClient) { }

  incrementLike(body): Observable<object> {
    console.log(body);
    return this.http.put(`${this.apiURL}liked`, body);
  }

  decrementLike(body): Observable<object> {
    console.log(body);
    return this.http.put(`${this.apiURL}unliked`, body);
  }
}
