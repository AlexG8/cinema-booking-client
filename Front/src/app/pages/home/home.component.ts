import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalTrailerComponent } from 'src/app/components/modals/modal-trailer/modal-trailer.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  movies: Array<any> = [];
  genres: Array<any>;

  selectedDate: Date;
  selectedFormat: string;

  formats: any[] = [
    {value: '', viewValue: 'Reinitialiser'},
    {value: '2D', viewValue: '2D'},
    {value: '4DX', viewValue: '4DX'},
    {value: 'IMAX', viewValue: 'IMAX'},
    {value: 'Numeric', viewValue: 'Numerique'},
    {value: '3D', viewValue: '3D'}
  ];

  filteredMovieScreenings: Array<any>;

  constructor(private moviesService: MoviesService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.getMovies();
  }

  getMovies(): void {
    this.moviesService.getMovies().subscribe((res: any) => this.movies = res.data);
  }

  openModal(trailer: string): void {
    const modalRef = this.modalService.open(ModalTrailerComponent,
      {
        scrollable: true,
        size: 'lg'
      });

    const data = {
      prop1: trailer,
    };

    modalRef.componentInstance.Trailer = data;
    modalRef.result.then((result) => {
    }, (err) => {
    });
  }

  filterByFormat(event): void {

    if (event.value === '') {
      return this.getMovies();
    }

    this.selectedFormat = event.value;
    const body = {
      format: this.selectedFormat,
      screening_date: this.selectedDate ? this.selectedDate.toISOString().split('T')[0] : null
    };
    this.moviesService.getMoviesByFormatAndDate(body).subscribe((res: any) => {
      this.movies = res.data;
      const uniqueArr = this.movies.reduce((acc, current) => {
        const x = acc.find(item => item.id === current.id);
        if (!x) {
          return acc.concat([current]);
        } else {
          return acc;
        }
      }, []);
      this.movies = uniqueArr;
    });
  }

  filterByScreeningDay(event): void {
    this.selectedDate = new Date(event.value);
    this.selectedDate.setDate(this.selectedDate.getDate() + 1);
    const body = {
      format: this.selectedFormat,
      screening_date: this.selectedDate.toISOString().split('T')[0]
    };
    this.moviesService.getMoviesByFormatAndDate(body).subscribe((res: any) => {
      this.movies = res.data;
      const uniqueArr = this.movies.reduce((acc, current) => {
        const x = acc.find(item => item.id === current.id);
        if (!x) {
          return acc.concat([current]);
        } else {
          return acc;
        }
      }, []);
      console.log(uniqueArr);
      this.movies = uniqueArr;
    });
  }
}
