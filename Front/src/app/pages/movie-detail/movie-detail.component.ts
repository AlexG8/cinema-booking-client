import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/services/users/auth.service';
import { MoviesService } from '../../services/movies.service';
import { UserService } from '../../services/users/user.service';
import {ReviewService} from '../../services/review.service';
import { Review } from '../../Models/review';
import { Movie } from '../../Models/movie';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss']
})
export class MovieDetailComponent implements OnInit {
  movie: Array<Movie>;
  idMovie: number;
  currentUserId: any;
  rating: number;

  formats: any[] = [
    {value: '', viewValue: 'Reinitialiser'},
    {value: '2D', viewValue: '2D'},
    {value: '4DX', viewValue: '4DX'},
    {value: 'IMAX', viewValue: 'IMAX'},
    {value: 'Numeric', viewValue: 'Numerique'},
    {value: '3D', viewValue: '3D'}
  ];


  constructor(private route: ActivatedRoute,
              private movieService: MoviesService,
              public sanitizer: DomSanitizer,
              public authService: AuthService,
              private modalService: NgbModal,
              private userService: UserService,
              private reviewService: ReviewService) {
    this.route.params.subscribe(params => this.idMovie = params.id);
    this.currentUserId = JSON.parse(this.authService.user);
  }

  ngOnInit(): void {
    this.getMovie();
    this.getMovieReviews();
  }

  getMovie(): void {
    this.movieService.getMovie(this.idMovie).subscribe((res: Movie) => this.movie = res.data);
  }

  getMovieReviews(): void {
    this.reviewService.getReviews(this.idMovie).subscribe((res: Review) => {
      if (res.data) {
        const ratingArr = res.data.map((review: Review) => review.rating);
        this.rating = this.averageRate(ratingArr);
      } else {
        this.rating = 0;
      }
    });
  }

  averageRate(nums): number {
    return nums.reduce((acc, val) => acc + val, 0) / nums.length;
  }}
