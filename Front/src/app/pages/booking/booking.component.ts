import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MoviesService} from '../../services/movies.service';
import { Screening } from '../../models/screening';
import {AuthService} from '../../services/users/auth.service';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit {
  idMovie: number;
  currentUser: any;
  movieScreenings: Array<any>;
  filteredMovieScreenings: Array<any>;
  screening: Array<Screening>;
  selectedSeat: Screening;
  selectedVouchId: number;
  loadingScreenings: boolean;
  showSeats: boolean;

  vouchs: any[] = [
    {value: '1', viewValue: 'Enfant'},
    {value: '2', viewValue: 'Etudiant'},
    {value: '3', viewValue: 'Adulte'},
    {value: '4', viewValue: 'Senior'}
  ];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private movieService: MoviesService, public authService: AuthService) {
    this.route.params.subscribe(params => this.idMovie = params.id);
    this.currentUser = JSON.parse(this.authService.user);
  }

  ngOnInit(): void {
    this.fetchMovieScreenings();
  }

  fetchMovieScreenings(): void {
    this.loadingScreenings = true;
    this.movieService.getMovieScreenings(this.idMovie).subscribe(
      (response: any) => {
        this.movieScreenings = response.data;
        this.filteredMovieScreenings = response.data;
      }
    );
    this.loadingScreenings = false;
  }

  fetchScreeningByFormat(format: string): Array<any> {
    this.showSeats = false;
    if (!format) {
      this.fetchMovieScreenings();
    }
    return this.filteredMovieScreenings = this.movieScreenings.filter(screenings => screenings.format === format);
  }

  getScreening(id: number): void {
    this.movieService.getScreening(id).subscribe(
      (response: any) => {
        this.screening = response.data;
        this.showSeats = true;
      }
    );
  }

  selectSeat(screeningSeat: Screening, userId = this.currentUser.data.user.id): void {
    if (screeningSeat.seat_statut === 'available') {
      const body = {
        seat_id: screeningSeat.seat_id,
        screening_id: screeningSeat.id,
        user_id: userId
      };
      this.movieService.seatSelected(body).subscribe(() => {
        screeningSeat.user_id = userId;
        screeningSeat.seat_statut = 'selected';
        this.selectedSeat = screeningSeat;

        setTimeout(() => {
          this.inactiveSeat(userId, this.selectedSeat);
        }, 500000);

        this.screening.forEach((screening: Screening) => {
          if (screening.seat_statut === 'selected' && screening.user_id === userId && screening.seat_id !== screeningSeat.seat_id) {
            const body = {
              seat_id: screening.seat_id,
              screening_id: screening.id
            };
            this.movieService.seatAvailable(body).subscribe(() => {
              screening.seat_statut = 'available';
            });
          }
        });
      });

    } else if (screeningSeat.seat_statut === 'selected' && screeningSeat.user_id === userId) {
      const body = {
        seat_id: screeningSeat.seat_id,
        screening_id: screeningSeat.id
      };
      this.movieService.seatAvailable(body).subscribe(() => {
        screeningSeat.seat_statut = 'available';
        this.selectedSeat = null;
      });
    }
  }

  inactiveSeat(currentUserId, screeningSeat): void {
    if (currentUserId === screeningSeat.user_id && screeningSeat.seat_statut === 'selected') {
      const body = {
        seat_id: screeningSeat.seat_id,
        screening_id: screeningSeat.id,
      };
      this.movieService.seatAvailable(body).subscribe(() => {
        screeningSeat.seat_statut = 'available';
        this.selectedSeat = null;
      });
    }
  }

  selectVouch(idVouch): void {
    this.selectedVouchId = parseInt(idVouch, 10);
  }

  bookScreening(): void {
    const body = {
      ticketIdentificaton: Date.now(),
      screening_id: this.selectedSeat.screening_id,
      user_id: this.currentUser.data.user.id,
      vouch_id: this.selectedVouchId,
      seat_id: this.selectedSeat.seat_id
    };

    this.movieService.bookScreening(body).subscribe((response: any) => {
      if (response.success) {
        this.seatOccupied({seat_id: body.seat_id, screening_id: body.screening_id, user_id: this.currentUser.data.user.id});
        this.selectedSeat.seat_statut = 'occupied';
        this.nbOfScreeningSeats({screening_id: body.screening_id});
        this.screening[0].nb_spectator = this.screening[0].nb_spectator - 1;
        this.router.navigate(['accueil/dashboard/reservation']);
      }
    });
  }

  seatOccupied(body): void {
    this.movieService.seatOccupied(body).subscribe(res => res);
  }

  nbOfScreeningSeats(body): void {
    this.movieService.nbOfScreeningSeats(body).subscribe(res => res);
  }
}
