const config = {
 API_BASE_URL: process.env.API_BASE_URL || 'http://localhost:8000/api/',
 AWS_S3_ACCESS_KEY: process.env.AWS_S3_ACCESS_KEY,
 AWS_S3_SECRET_KEY: process.env.AES_S3_SECRET_KEY
}

module.exports = config;
